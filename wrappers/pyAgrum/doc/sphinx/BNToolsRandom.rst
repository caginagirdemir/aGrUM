Generation of Bayesian network
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autofunction:: pyAgrum.randomBN
  :noindex:

.. autoclass:: pyAgrum.BNGenerator